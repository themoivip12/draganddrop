import React from "react";
import logo from "./logo.svg";
import "./App.css";
import { DragDropContext, DropResult } from "react-beautiful-dnd";
import styled from "styled-components";
import DraggableElement from "./DragableElement";
import { Flex, ListGrid } from "./style";

export interface Player {
  content: string;
  id: string;
  prefix: string;
}

const getItems = (count: number, prefix: string) =>
  Array.from({ length: count }, (v, k) => k).map((k) => {
    const randomId = Math.floor(Math.random() * 1000);
    return {
      id: `item-${randomId}`,
      prefix,
      content: `caauf thu ${randomId}`,
    };
  });

const removeFromList = (list: any[], index: number) => {
  const result = Array.from(list);
  const [removed] = result.splice(index, 1);
  return [removed, result];
};

const addToList = (list: any[], index: number, element: any) => {
  const result = Array.from(list);
  result.splice(index, 0, element);
  return result;
};

const lists = ["GK", "CB", "CM", "CF"];

const generateLists = (): Record<string, Player[]> => {
  return {
    GK: [{ content: "cầu thu 1", id: "item-1", prefix: "CB" }],
    CB: [
      { content: "cầu thu 2", id: "item-2", prefix: "CB" },
      { content: "cầu thu 3", id: "item-3", prefix: "CB" },
      { content: "cầu thu 4", id: "item-4", prefix: "CB" },
      { content: "cầu thu 5", id: "item-5", prefix: "CB" },
      { content: "cầu thu 6", id: "item-6", prefix: "CB" },
    ],
    CM: [
      { content: "cầu thu 7", id: "item-7", prefix: "CB" },
      { content: "cầu thu 8", id: "item-8", prefix: "CB" },
      { content: "cầu thu 9", id: "item-9", prefix: "CB" },
    ],
    CF: [
      { content: "cầu thu 10", id: "item-10", prefix: "CB" },
      { content: "cầu thu 11", id: "item-11", prefix: "CB" },
    ],
  };
};

const generateListsEnemy = (): Record<string, Player[]> => {
  return {
    GK: [{ content: "cầu thu khách 1", id: "item-e-1", prefix: "CB" }],
    CB: [
      { content: "cầu thu khách 2", id: "item-e-2", prefix: "CB" },
      { content: "cầu thu khách 3", id: "item-e-3", prefix: "CB" },
      { content: "cầu thu khách 4", id: "item-e-4", prefix: "CB" },
      { content: "cầu thu khách 5", id: "item-e-5", prefix: "CB" },
    ],
    CM: [
      { content: "cầu thu khách 6", id: "item-e-6", prefix: "CB" },
      { content: "cầu thu khách 7", id: "item-e-7", prefix: "CB" },
      { content: "cầu thu khách 8", id: "item-e-8", prefix: "CB" },
      { content: "cầu thu khách 9", id: "item-e-9", prefix: "CB" },
    ],
    CF: [
      { content: "cầu thu khách 10", id: "item-e-10", prefix: "CB" },
      { content: "cầu thu khách 11", id: "item-e-11", prefix: "CB" },
    ],
  };
};

function App() {
  const [elements, setElements] = React.useState<Record<string, Player[]>>(
    generateLists()
  );
  console.log(133, generateLists());

  const [elementsS, setElementsS] = React.useState<Record<string, Player[]>>(
    generateListsEnemy()
  );

  console.log(elements);

  const onDragEnd = (result: DropResult) => {
    if (!result.destination) {
      return;
    }
    const listCopy = { ...elements };

    const sourceList = listCopy[result.source.droppableId];
    const [removedElement, newSourceList] = removeFromList(
      sourceList,
      result.source.index
    );
    listCopy[result.source.droppableId] = newSourceList;
    const destinationList = listCopy[result.destination.droppableId];
    listCopy[result.destination.droppableId] = addToList(
      destinationList,
      result.destination.index,
      removedElement
    );

    setElements(listCopy);
  };

  const onDragEndS = (result: DropResult) => {
    if (!result.destination) {
      return;
    }
    const listCopy = { ...elementsS };

    const sourceList = listCopy[result.source.droppableId];
    const [removedElement, newSourceList] = removeFromList(
      sourceList,
      result.source.index
    );
    listCopy[result.source.droppableId] = newSourceList;
    const destinationList = listCopy[result.destination.droppableId];
    listCopy[result.destination.droppableId] = addToList(
      destinationList,
      result.destination.index,
      removedElement
    );

    setElementsS(listCopy);
  };

  return (
    <Flex>
      <DragDropContext onDragEnd={onDragEnd}>
        <ListGrid>
          {lists.map((listKey) => (
            <DraggableElement
              elements={elements[listKey]}
              key={listKey}
              prefix={listKey}
            />
          ))}
        </ListGrid>
      </DragDropContext>
      <DragDropContext onDragEnd={onDragEndS}>
        <ListGrid>
          {lists.map((listKey) => (
            <DraggableElement
              elements={elementsS[listKey]}
              key={listKey}
              prefix={listKey}
            />
          ))}
        </ListGrid>
      </DragDropContext>
    </Flex>
  );
}

export default App;
