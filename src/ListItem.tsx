import { Draggable, DraggableChildrenFn } from "react-beautiful-dnd";

import styled from "styled-components";
import { Player } from "./App";

const CardHeader = styled.div`
  font-weight: 500;
`;

const Author = styled.div`
  display: flex;
  align-items: center;
`;
const CardFooter = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const DragItem = styled.div<DraggableChildrenFn>`
  padding: 10px;
  border-radius: 6px;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  background: white;
  margin: 0 0 8px 0;
  display: grid;
  grid-gap: 20px;
  flex-direction: column;
`;

const ListItem = ({ item, index }: { item: Player; index: number }) => {
  return (
    <Draggable
      isDragDisabled={item.prefix === "GK"}
      draggableId={item.id}
      index={index}
      disableInteractiveElementBlocking={item.prefix === "GK"}
    >
      {(provided, snapshot) => {
        return (
          <DragItem
            ref={provided.innerRef}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
          >
            <div>
              <CardHeader>{item.id}</CardHeader>
              <span>Content</span>
              <CardFooter>
                <span>{item.content}</span>
                <Author>
                  {item.id}
                  <p>avatar</p>
                </Author>
              </CardFooter>
            </div>
          </DragItem>
        );
      }}
    </Draggable>
  );
};

export default ListItem;
